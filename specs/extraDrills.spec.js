describe('Extra Drills', () => {
    it('should return a count of each appearance of letters in a given list', () => {
        expect(getCountOfLetters(['apple', 'pear'])).toEqual({a: 2, p: 3, l:1, e: 2, r: 1});
    });

    describe('Not for Children', () => {
        it('should return false with message if the person is under 18', () => {
            expect(isLegalAge(15)).toEqual([false, 'You are under age.']);
        });
        it('should return true with message if the person is over 21', () => {
            expect(isLegalAge(25)).toEqual([true, 'You are permitted to enter and drink.'])
        });
        it('should return true with exception if person is between 18-21', () => {
            expect(isLegalAge(20)).toEqual([false, 'You may enter, but cannot purchase alcohol.']);
        });
    });

    describe('Make the Grade', () => {
        it('should return A if the score is between 93-100', () => {
            expect(getGrade(95)).toEqual('A');
        });
        it('should return B if the score is between 85-92', () => {
            expect(getGrade(90)).toEqual('B');
        });
        it('should return C if the score is between 77-84', () => {
            expect(getGrade(79)).toEqual('C');
        });
        it('should return D if the score is between 69-76', () => {
            expect(getGrade(69)).toEqual('D');
        });
        it('should return F if the score is at or below 68', () => {
            expect(getGrade(68)).toEqual('F');
        });
    });

    describe('The Calculator', () => {
        let calculator;
        beforeEach(() => {
            calculator = new Calculator();
        });

        it('should add 2 numbers', () => {
            expect(calculator.sum(2, 1)).toEqual(3);
        });
        it('should subtract 2 numbers', () => {
            expect(calculator.subtract(2, 1)).toEqual(1);
        });
        it('should divide 2 numbers', () => {
            expect(calculator.divide(16, 4)).toEqual(4);
        });
        it('should multiply 2 numbers', () => {
            expect(calculator.multiply(17, 2)).toEqual(34);
        });
    });

    describe('The CleanBot', () => {
        let cleanBot;

        it('should add valid tasks (mop, sweep, dust, polish) to its task list', () => {
            cleanBot = new CleanBot();
            cleanBot.addTask('scrub');
            cleanBot.addTask('mop');
            expect(cleanBot.tasks).toEqual(['mop']);
        });

        it('should clean when given a new task list', () => {
            cleanBot = new CleanBot();
            cleanBot.addTask('mop');
            cleanBot.clean(['dust', 'sweep'])
            expect(cleanBot.tasks).toEqual(['mop']);
        });

        it('should clean its current task list', () => {
            cleanBot = new CleanBot();
            cleanBot.addTask(['dust']);
            cleanBot.clean();
            expect(cleanBot.tasks).toEqual([]);
        });

        xit('should call each operation given a valid task', () => {
            cleanBot = new CleanBot();
            spyOn(cleanBot, 'mop');
            cleanBot.addTask('mop');
            cleanBot.clean()
            expect(cleanBot.mop).toHaveBeenCalledTimes(1);
        });

    });
})
