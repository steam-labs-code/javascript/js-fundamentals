# Javascript Fundamentals

This project contains exercises to hone your Javascript fundamentals:

- Variables
- Functions
- Control structures
- Conditionals
- Basic CLI/Node CLI
- Debugger

## Dev Setup

**VS Code**

A `launch.json` has already been configured in this repository. Feel free to adjust the setings or add new configurations.

Debugging a web app is a little different than debugging a simple script.


### Debugging Scripts

VS Code can launch your app from the debugger. 


### Debug Console

The Debug Console in VS Code is very similar to the console you use in the web browser. It provides a command prompt where you can type in any valid Javscript expressions to test it out.

This is a powerful tool. Use it to experiment with variables, data structures, functions and methods as you work.

