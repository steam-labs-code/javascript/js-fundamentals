describe('Number Drills', () => {
    it('should return whether a given argument is a valid number', () => {
        expect(isValidNumber(1)).toBeTrue();
        expect(isValidNumber('1')).toBeTrue();
        expect(isValidNumber('one')).toBeFalse();
    });

    it('should return the sum of 2 numbers', () => {
        expect(sum(2, 7)).toEqual(9);
        expect(sum(209, 209)).toEqual(418);
        expect(sum('32', '8')).toEqual(40);
    });

    it('should return an array of all numbers evenly divisible by a given number', () => {
        expect(divisibleBy(10, 5)).toEqual([5, 10]);
        expect(divisibleBy(35, 8)).toEqual([8, 16, 24, 32]);
    });

    it('should return whether a given number is prime', () => {
        expect(isPrime(2)).toBeTrue();
        expect(isPrime(10)).toBeFalse();
        expect(isPrime(103)).toBeTrue();
        expect(isPrime(-103)).toBeFalse();
    });

    it('should return the factorial of a given number', () => {
        expect(factorial(3)).toEqual(6);
        expect(factorial(5)).toEqual(120);
        expect(factorial(-3)).toEqual(undefined);
        expect(factorial(0)).toEqual(1);
    });

    it('should return a list of prime numbers up to a given maximum number', () => {
        expect(primesTo(10)).toEqual([2, 3, 5, 7]);
        expect(primesTo(50)).toEqual([2, 3,5,7,11,13,17,19,23,29,31,37,41,43,47]);
    });

    it('should return the highest number in a given array', () => {
        expect(highest([11, 3, 76, 25, 13])).toEqual(76);
        expect(highest([11, 3, '76', 25, 13])).toEqual(76);
    });

    it('should convert a an imperial unit to a given metric unit', () => {
        expect(convertToMeters(1, 'in', 'mm')).toEqual(25.4)
        expect(convertToMeters(4, 'ft', 'm')).toEqual(1.2);
    });

    it('should calculate total price given a price and tax percentage', () => {
        expect(getTotalPrice(3.75, 'CA')).toEqual(4.03);
        expect(getTotalPrice(17.91, 'TX')).toEqual(19.03);
    });
})
