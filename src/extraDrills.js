/**
 * Instructions
 * ============
 *
 * Complete the functions below to make all of the tests pass. To see which tests are failing, open `SpecRunner.html` in your browser or use the VS Code debugger to run the Test Report.
 *
 * Resources
 * =========
 *
 * 1 - Javascript Basic Examples:
 * https://developer.mozilla.org/en-US/docs/MDN/Guidelines/Code_guidelines/JavaScript
 *
 * -------------------------------------------------------------------------
 *
 * 2 - Docs:
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects
 *
 * -------------------------------------------------------------------------
 *
 * 3 - Try it out! See what happens when you manipulate strings by trying each string method in the console.
 */

// LEVEL 1 CHALLENGES
// -----------------------------------------------------------------------

/**
 * Given a list of words, return a total count how often each letter of each word in it.
 * @param {array} words
 * @returns {object}
 */
function getCountOfLetters(words) {}

/**
 * Given an age, return whether the person is of legal drinking age in USA.
 * @param {number} age
 * @returns {array} containing a boolean and a message
 */
function isLegalAge(age) {}

/**
 * Given a student's score, return a letter grade.
 * @param {number} grade
 * @returns {string}
 */
function getGrade(grade) {}

/**
 * Given a multi-dimensional array, print a tic-tac-toe board using only 1 loop.
 */
function printTicTacToe() {}

// Calculator
class Calculator {}
