describe('Array Drills', () => {
    it('should return the total number of items in an array', () => {
        expect(itemCount([12, 34, 56])).toEqual(3);
    });

    it('should return a list of the indexes of each item', () => {
        expect(printIndices(['one', 'two', 'three'])).toEqual([0, 1, 2])
    });

    it('should return a sorted array that was merged from 2 arrays', () => {
        expect(merge([1, 2, 3], [4, 5, 6])).toEqual([1, 2, 3, 4, 5, 6])
    });

    it('should return a sorted number array', () => {
        expect(sortItems([54, 3, 17, 42])).toEqual([3, 17, 42, 54])
    });

    it('should return whether a given array contains duplicate items', () => {
        expect(hasDuplicates(['fruit', 'vegetable', 'fat', 'fruit'])).toBeTrue();
    });

    it('should return an array of every other item in a given array', () => {
        expect(everyOtherItem([3, 6, 9, 12])).toEqual([3, 9])
    });

    it('should return the lowest N numbers given an array and a number', () => {
        expect(smallestNItems([14, 3, 78, 26, 9, 52], 3)).toEqual([3, 9, 14]);
    });

    it('should append only valid numbers to a given array', () => {
        let numberArray = [1,2,3];
        const actualSuccess = addNumberToArray(numberArray, '978');
        const actualNoChange = addNumberToArray(numberArray, 'one');
        expect(actualSuccess).toEqual([1,2,3,978])
        expect(actualNoChange).toEqual(numberArray)
    });

    it('should return an array of fibonacci numbers up to a given number', () => {
        expect(fibonacci(10)).toEqual([0, 1, 1, 2, 3, 5, 8])
    });

    it('should return an array of common words between 2 arrays', () => {
        expect(getCommonWords(['door', 'window', 'floor'], ['window', 'roof', 'sink'])).toEqual(['window']);
    });

    it('should return an array with the sums of each index of 2 arrays', () => {
        expect(sumOfArrays([1, 2, 3], [4, 5, 6])).toEqual(5, 7, 9);
    });

});
