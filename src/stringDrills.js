/**
 * Instructions
 * ============
 * 
 * Complete the functions below to make all of the tests pass. To see which tests are failing, open `SpecRunner.html` in your browser or use the VS Code debugger to run the Test Report.
 * 
 * Resources
 * =========
 * 
 * 1 - Javascript Basic Examples:
 * https://developer.mozilla.org/en-US/docs/MDN/Guidelines/Code_guidelines/JavaScript
 * 
 * -------------------------------------------------------------------------
 * 
 * 2 - String Methods:
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String
 * 
 * -------------------------------------------------------------------------
 * 
 * 3 - Try it out! See what happens when you manipulate strings by trying each string method in the console. 
 */


/**
 * Given a string, return the total number of characters in it.
 * @param {string} arg - any string of any length
 * @returns number
 */
function characterCount(arg) {}


/**
 * Given a string, return the total number of words in the string. 
 * @param {string} arg - any string of any length
 * @returns {number} - total number of words
 */
function wordCount(arg) {}


/**
 * Given a string and a word, return true if the string contains the word.
 * @param {string} string - a string to search
 * @param {string} word - the word to find within a string
 * @returns {boolean} - does the string contain the word?
 */
function containsWord(string, word) {}


/**
 * Given a string, return the total number of sentences in it.
 * @param {string} arg - any string of any length
 * @returns {number} - the total number of sentences
 */
function sentenceCount(arg) {}


/**
 * Given a string, return the total number of capitalized words contained within. Assume the string contains no abbreviations (such as Mr. or U.K.).
 * @param {string} arg - any string of any length
 * @returns {number} - the total number of words
 */
function capitalizedWordCount(arg) {}


/**
 * Given a string, return the total number of vowels contained within.
 * @param {string} arg - any string of any length
 * @returns {number} - the total number of vowels
 */
function vowelCount(arg) {}


/**
 * Given a word, return the first and last letter. Assume the string only contains 1 word.
 * @param {string} word - a string containing 1 word
 * @returns {string} - 2 letters of the first and last letter of the word
 */
function firstAndLastLetter(arg) {}


/**
 * Given a string, swap the first letter of each word and return the 2 new words.
 * @param {string} twoWordArg - a string containg 2 words
 * @returns {string} - a string with 2 words
 */
function swapFirstLetter(twoWordArg) {}

/**
 * Given a string, use the very first letter and return a new string with each subsequent appearance of that letter to an asterisk (*).
 * @param {string} arg - any string of any length.
 * @param {string} - a string 
 */
function censorText(arg) {}


/**
 * CHALLENGE:
 * 
 * Given a string, print a full report to the console. The report should detail all of the information above.
 */

/**
 * CHALLENGE: 
 * 
 * Modify `sentenceCount()` to accept strings that contain abbreviations.
 */