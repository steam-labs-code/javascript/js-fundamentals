describe('String Practice', () => {
    it('should return the total number of characters in a given string', () => {
        const stringWith15Letters = 'Testing is fun!';
        const stringWith3Spaces = '   ';
        expect(characterCount(stringWith15Letters)).toEqual(15);
        expect(characterCount(stringWith3Spaces)).toEqual(3);
    });

    it('should return the total number of words in a given string', () => {
        const stringWith5Words = 'I love chocolate with everything.'
        expect(wordCount(stringWith5Words)).toEqual(5);
    });

    it('should return true if a string contains a given word', () => {
        expect(containsWord('over the rainbow', 'banana')).toBeFalse();
        expect(containsWord('Sam I am', 'am')).toBeTrue();
    });

    it('should return the total number of sentences in a text (a very large string)', () => {
        expect(sentenceCount(shortStory)).toEqual(8);
    });

    it('should return the total number of vowels in a string.', () => {
        expect(vowelCount('aeiou')).toEqual(5);
    });

    it('should return the first and last letter of a string', () => {
        expect(firstAndLastLetter('woodcutter')).toEqual('wr');
    });

    it('should swap the first letter of each word in a string with 2 words', () => {
        expect(swapFirstLetter('sunny day')).toEqual ('dunny say');
    });

    it('should return the total number of capitalized letters in a string', () => {
        expect(capitalizedWordCount('I Am The Goddess Of War')).toEqual(6);

    });

    it('should replace subsequent occurrences of the first letter of a string'), () => {
        const actual = censorText('trains travel to tokyo');
        const expected = 'trains *ravel *o *okyo';
        expect(actual).toEqual(expected);
    };
})
