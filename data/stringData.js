const shortStory = `Andy Warhol said that "In the future, everyone will be
famous for fifteen minutes".  The computer nets, more than
any other medium, lend credibility to this prediction.  A
network conference may span the globe more completely than
even satellite TV, yet be open to anyone who can afford the
simplest computer and modem. Through our participation in
conferencing, each of us becomes, if only briefly, a public
figure of sorts -- often without realizing it, and without
any contemplation of the implications and possible
consequences.

Brian Reid (reid@decwrl.DEC.COM) conducts and distributes
periodic surveys of Usenet conference readership.  His
statistical results for the end of 1991 show that of the
1,459 conferences which currently make up Usenet, more than
fifty percent have over 20,000 readers apiece; the most
popular conferences are each seen by about 200,000 readers!
Mr. Reid's estimate of total Usenet readership is nearly TWO
MILLION people.

Note that Mr. Reid's numbers are for Usenet only; they do
not include any information on other large public nets such
as RIME (PC-Relaynet), Fido, or dozens of others, nor do
they take into account thousands of private networks which
may have indirect public network connections. The total
number of users with access to public networks is unknown,
but informed estimates range to the tens of millions, and
the number keeps growing at an amazing pace -- in fact, the
rate of growth of this medium may be greater than any other
communications medium in history`;

const shortParagraph = `The Apple Cat // modem is by far the most expandable modem on the market
today.	Of course it's also the choice modem of pirates because of it's
inexpensive half-duplex 1200 baud capabilities.  The expansion module available
for the cat has several very useful functions.	Rather than shelling out $30
bucks for one which you may only use a few of the features this file tells you
how to build just certain features or even the whole package.`;
