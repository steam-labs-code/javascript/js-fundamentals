/**
 * Instructions
 * ============
 *
 * Complete the functions below to make all of the tests pass. To see which tests are failing, open `SpecRunner.html` in your browser or use the VS Code debugger to run the Test Report.
 *
 * Resources
 * =========
 *
 * 1 - Javascript Basic Examples:
 * https://developer.mozilla.org/en-US/docs/MDN/Guidelines/Code_guidelines/JavaScript
 *
 * -------------------------------------------------------------------------
 *
 * 2 - Array Methods:
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array
 *
 * -------------------------------------------------------------------------
 *
 * 3 - Try it out! See what happens when you manipulate strings by trying each string method in the console.
 */

// LEVEL 1 CHALLENGES
// -----------------------------------------------------------------------

/**
 * Given an array, return a count of the items within.
 * @param {array} items
 * @returns {array}
 */
function itemCount(items) {}

/**
 * Given an array, return an array the indices of each item.
 * @param items
 * @returns {array}
 */
function printIndices(items) {}

/**
 * Given a pair of arrays, merge them into 1 array
 * @param {array} arr1
 * @param {array} arr2
 * @returns {array}
 */
function merge(arr1, arr2) {}

/**
 * Given a list of numbers, sort them from lowest to highest.
 * @param {array} items
 * @returns {array}
 */
function sortItems(items) {}

// LEVEL 2 CHALLENGES
// -----------------------------------------------------------------------
/**
 * Given an array, check if there are multiple appearances of any items.
 * @param {array} items
 * @returns {boolean}
 */
function hasDuplicates(items) {}

/**
 * Given an array, return every other item in it.
 * @param {array} items
 * @returns {array}
 */
function everyOtherItem(items) {}

/**
 *
 * @param {array} items
 * @param {array} numOfItemsToReturn
 * @returns {array}
 */
function smallestNItems(items, numOfItemsToReturn) {}

/**
 *
 * @param {array} items
 * @param {array} arg
 * @returns {array}
 */
function addNumberToArray(items, arg) {}

/**
 * Given
 * https://www.mathsisfun.com/numbers/fibonacci-sequence.html
 * @param num
 */
function fibonacci(num) {}

// LEVEL 3 CHALLENGES
// -----------------------------------------------------------------------
/**
 *
 * @param {array} arr1
 * @param {array} arr2
 * @returns {array}
 */
function getCommonWords(arr1, arr2) {}

/**
 *
 * @param {array} list1
 * @param {array} list2
 * @returns {array}
 */
function sumOfArrays(list1, list2) {}

// OPTIONAL CHALLENGES
// -----------------------------------------------------------------------
