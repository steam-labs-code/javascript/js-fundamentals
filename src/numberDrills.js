/**
 * Instructions
 * ============
 *
 * Complete the functions below to make all of the tests pass. To see which tests are failing, open `SpecRunner.html` in your browser or use the VS Code debugger to run the Test Report.
 *
 * Resources
 * =========
 *
 * 1 - Javascript Basic Examples:
 * https://developer.mozilla.org/en-US/docs/MDN/Guidelines/Code_guidelines/JavaScript
 *
 * -------------------------------------------------------------------------
 *
 * 2 - Number Operations:
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number
 *
 * -------------------------------------------------------------------------
 *
 * 3 - Try it out! See what happens when you manipulate strings by trying each string method in the console.
 */

// LEVEL 1 CHALLENGES
// -----------------------------------------------------------------------
function isValidNumber(arg) {}

function sum(num1, num2) {}

function divisibleBy(num, divisor) {}

// LEVEL 2 CHALLENGES
// -----------------------------------------------------------------------
/**
 * Given a number, return whether it is prime or not.
 * https://www.mathsisfun.com/prime_numbers.html
 * @param {number} num
 * @returns {boolean}
 */
function isPrime(num) {}

/**
 * Given a number, return its factorial.
 * https://www.mathsisfun.com/numbers/factorial.html
 * @param num
 */
function factorial(num) {}

function primesTo(num) {}

function highest(nums) {}

// LEVEL 3 CHALLENGES
// -----------------------------------------------------------------------

/**
 * Given an imperial measurement of length, return its metric equivalent.
 * https://www.gigacalculator.com/converters/imperial-to-metric.php
 * @param {number} value
 * @param {string} imperialUnit
 * @param {string} metricUnit
 * @returns {float} metric value rounded to the nearest tenth
 */
function convertToMeters(value, imperialUnit, metricUnit) {}

/**
 * Given a price and state, return the total price including sales tax.
 * https://www.tax-rates.org/taxtables/sales-tax-by-state
 * @param {number} price
 * @param {string} state
 * @returns {number} total price rounded to the nearest hundredth
 */
function getTotalPrice(price, state) {}

// OPTIONAL CHALLENGES
// -----------------------------------------------------------------------

/**
 * CHALLENGE
 *
 * Add a `factorial()` method to the Number class.
 */

